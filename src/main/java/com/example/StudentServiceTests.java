package com.example;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.StudentModel;
import com.example.service.StudentService;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StudentServiceTests
{
	@Autowired
	StudentService service;
	
	@Test
	public void testSelectAllStudents()
	{
		List<StudentModel> students = service.selectAllStudents();
		Assert.assertNotNull("Gagal = student menghasilkan null", students);
		Assert.assertEquals("Gagal - size students tidak sesuai", 4, students.size());
	}
	
	@Test
	public void testSelectStudent()
	{
		// cek mahasiswa hienzo nama = hienzo, npm = 124, gpa = 1.2 ada didatabase
		StudentModel student = service.selectStudent("124");
		
		Assert.assertNotNull("Gagal = student menghasilkan null", student);
		Assert.assertEquals("Gagal = nama student tidak sesuai", "hienzo", student.getName());
		Assert.assertEquals("Gagal - GPA student tidak sesuai", 1.2, student.getGpa(), 0.0);
	}
	
	@Test
	public void testCreateStudent()
	{
		// cek mahasiswa nama = hienzo, npm = 124, gpa = 1.2
		//StudentModel student = new StudentModel("124", "hienzo", 1.3, null);
		StudentModel student = new StudentModel("126", "Budi", 3.44, null);
		
		// Cek apakah student sudah ada
		Assert.assertNull("Mahasiswa sudah ada", service.selectStudent(student.getNpm()));
		
		// Masukkan ke service
		service.addStudent(student);
		
		// Cek apakah student berhasil dimasukkan
		Assert.assertNotNull("Mahasiswa gagal dimasukkan", service.selectStudent(student.getNpm()));
	}
	
	@Test
	public void testUpdateStudent()
	{
		// cek mahasiswa nama = hienzo, npm = 124, gpa = 1.2
		// Ambillah suatu StudentModel dengan npm tertentu (bebas)
		StudentModel student = new StudentModel("126", "Budi", 3.44, null);
		
		// Ubah GPA atau nama dari student model tersebut
		student.setName("Bejo");
		
		// Update student menggunakan method yang ada di service
		service.updateStudent(student);
		
		// Ambil kembali student dengan menggunakan method yang ada di service
		service.selectStudent("126");
		
		// Cek apakah student sudah ada
		Assert.assertNull("Mahasiswa sudah ada", service.selectStudent(student.getNpm()));

		// Cek apakah atribut yang udah tadi ikut berubah
		Assert.assertEquals("Gagal = nama student tidak sesuai", "Bejo", student.getName());
	}
	
	@Test
	public void testDeleteStudent()
	{
		// mahasiswa nama = hienzo, npm = 124, gpa = 1.2
		StudentModel student = new StudentModel("124", "hienzo", 1.2, null);
		
		// Cek bahwa mahasiswa yang diambil tidak null
		Assert.assertNull("Student tidak null", service.selectStudent(student.getNpm()));
		
		// Delete student dengan menggunakan method yang ada di service
		service.deleteStudent(student.getNpm());
		
		// Ambil kembali student dengan npm yang sama menggunakan method yang ada di service
		service.selectStudent(student.getNpm());
		
		// Cek apakah mahasiswa yang diambil null
		Assert.assertNotNull("Student menghasilkan null", student);
	}
}