package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Many;

import com.example.model.CourseModel;
import com.example.model.StudentModel;

@Mapper
public interface StudentMapper
{ 
	// tutorial 4 dulu
    // @Select("select npm, name, gpa from student where npm = #{npm}")
    //StudentModel selectStudent (@Param("npm") String npm);

    @Select("select npm, name, gpa from student")
    @Results(value = {
    	    @Result(property="npm", column="npm"),
    	    @Result(property="name", column="name"),
    	    @Result(property="gpa", column="gpa"),
    	    @Result(property="courses", column="npm",
    		    javaType = List.class,
    		    many=@Many(select="selectCourses"))
    	    })
    List<StudentModel> selectAllStudents ();
    
    

    @Insert("INSERT INTO student (npm, name, gpa) VALUES (#{npm}, #{name}, #{gpa})")
    void addStudent (StudentModel student);
    
    @Delete("DELETE FROM student WHERE npm = #{npm}")
    void deleteStudent (@Param("npm") String npm);
    
    @Update("UPDATE student SET npm = #{npm}, name = #{name}, gpa = #{gpa} WHERE npm = #{npm}")
    void updateStudent (StudentModel student);
    
    // TUTORIAL 5
    // tutorial 5 baru
    @Select("select course.id_course, nama, sks " +
    		"from studentcourse join course " +
    		"on studentcourse.id_course = course.id_course " +
    		"where studentcourse.npm = #{npm}")
    List<CourseModel> selectCourses (@Param("npm") String npm);
    
    // tutorial 5 baru
    @Select("select npm, name, gpa from student where npm = #{npm}")
	@Results(value = {
	    @Result(property="npm", column="npm"),
	    @Result(property="name", column="name"),
	    @Result(property="gpa", column="gpa"),
	    @Result(property="courses", column="npm",
		    javaType = List.class,
		    many=@Many(select="selectCourses"))
	    })
    StudentModel selectStudent (@Param("npm") String npm);
    
    // menampilkan matakuliah tertentu 
    // dan mahasiswa yang mengambilnya
    @Select("select id_course, nama, sks from course where id_course = #{id_course}")
	@Results(value = {
	    @Result(property="idCourse", column="id_course"),
	    @Result(property="nama", column="nama"),
	    @Result(property="sks", column="sks"),
	    @Result(property="students", column="id_course",
		    javaType = List.class,
		    many=@Many(select="selectStudentCourses"))
	    })
    CourseModel selectMakul (@Param("id_course") String id);
    
    // select matakuliah
    @Select("select student.name, student.npm " +
    		"from studentcourse join student " +
    		"on studentcourse.npm = student.npm " +
    		"where studentcourse.id_course = #{id_course}")
    List<StudentModel> selectStudentCourses (@Param("id_course") String id);
}
